<?php
/**
 * BaseEvent
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  Compucie\Congressus
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Congressus API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 3.0
 * Contact: support@congressus.nl
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Compucie\Congressus\Model;

use \ArrayAccess;
use \Compucie\Congressus\ObjectSerializer;

/**
 * BaseEvent Class Doc Comment
 *
 * @category Class
 * @package  Compucie\Congressus
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class BaseEvent implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'BaseEvent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'category_id' => 'int',
        'category' => '\Compucie\Congressus\Model\EventCategoryBase',
        'status' => 'string',
        'slug' => 'string',
        'name' => 'string',
        'description' => 'string',
        'published' => 'bool',
        'visibility' => 'string',
        'authentication_required' => 'bool',
        'start' => '\DateTime',
        'end' => '\DateTime',
        'whole_day' => 'bool',
        'location' => 'string',
        'num_tickets' => 'int',
        'num_tickets_sold' => 'int',
        'website_url' => 'string',
        'website_subscribe_url' => 'string',
        'comments_open' => 'bool',
        'comments' => '\Compucie\Congressus\Model\EventComment[]',
        'media' => '\Compucie\Congressus\Model\StorageObject[]',
        'memo' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'id' => null,
        'category_id' => null,
        'category' => null,
        'status' => null,
        'slug' => null,
        'name' => null,
        'description' => null,
        'published' => null,
        'visibility' => null,
        'authentication_required' => null,
        'start' => 'date-time',
        'end' => 'date-time',
        'whole_day' => null,
        'location' => null,
        'num_tickets' => null,
        'num_tickets_sold' => null,
        'website_url' => 'url',
        'website_subscribe_url' => 'url',
        'comments_open' => null,
        'comments' => null,
        'media' => null,
        'memo' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'id' => false,
        'category_id' => false,
        'category' => false,
        'status' => false,
        'slug' => true,
        'name' => false,
        'description' => true,
        'published' => false,
        'visibility' => false,
        'authentication_required' => false,
        'start' => false,
        'end' => false,
        'whole_day' => true,
        'location' => true,
        'num_tickets' => true,
        'num_tickets_sold' => true,
        'website_url' => false,
        'website_subscribe_url' => false,
        'comments_open' => true,
        'comments' => true,
        'media' => true,
        'memo' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'category_id' => 'category_id',
        'category' => 'category',
        'status' => 'status',
        'slug' => 'slug',
        'name' => 'name',
        'description' => 'description',
        'published' => 'published',
        'visibility' => 'visibility',
        'authentication_required' => 'authentication_required',
        'start' => 'start',
        'end' => 'end',
        'whole_day' => 'whole_day',
        'location' => 'location',
        'num_tickets' => 'num_tickets',
        'num_tickets_sold' => 'num_tickets_sold',
        'website_url' => 'website_url',
        'website_subscribe_url' => 'website_subscribe_url',
        'comments_open' => 'comments_open',
        'comments' => 'comments',
        'media' => 'media',
        'memo' => 'memo'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'category_id' => 'setCategoryId',
        'category' => 'setCategory',
        'status' => 'setStatus',
        'slug' => 'setSlug',
        'name' => 'setName',
        'description' => 'setDescription',
        'published' => 'setPublished',
        'visibility' => 'setVisibility',
        'authentication_required' => 'setAuthenticationRequired',
        'start' => 'setStart',
        'end' => 'setEnd',
        'whole_day' => 'setWholeDay',
        'location' => 'setLocation',
        'num_tickets' => 'setNumTickets',
        'num_tickets_sold' => 'setNumTicketsSold',
        'website_url' => 'setWebsiteUrl',
        'website_subscribe_url' => 'setWebsiteSubscribeUrl',
        'comments_open' => 'setCommentsOpen',
        'comments' => 'setComments',
        'media' => 'setMedia',
        'memo' => 'setMemo'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'category_id' => 'getCategoryId',
        'category' => 'getCategory',
        'status' => 'getStatus',
        'slug' => 'getSlug',
        'name' => 'getName',
        'description' => 'getDescription',
        'published' => 'getPublished',
        'visibility' => 'getVisibility',
        'authentication_required' => 'getAuthenticationRequired',
        'start' => 'getStart',
        'end' => 'getEnd',
        'whole_day' => 'getWholeDay',
        'location' => 'getLocation',
        'num_tickets' => 'getNumTickets',
        'num_tickets_sold' => 'getNumTicketsSold',
        'website_url' => 'getWebsiteUrl',
        'website_subscribe_url' => 'getWebsiteSubscribeUrl',
        'comments_open' => 'getCommentsOpen',
        'comments' => 'getComments',
        'media' => 'getMedia',
        'memo' => 'getMemo'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    public const STATUS_ON_SALE = 'on sale';
    public const STATUS_WAITING_LIST = 'waiting list';
    public const STATUS_SOLD_OUT = 'sold out';
    public const STATUS_NOT_ON_SALE = 'not on sale';
    public const STATUS_NO_PARTICIPATION = 'no participation';
    public const VISIBILITY__PUBLIC = 'public';
    public const VISIBILITY__PROTECTED = 'protected';
    public const VISIBILITY__PRIVATE = 'private';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_ON_SALE,
            self::STATUS_WAITING_LIST,
            self::STATUS_SOLD_OUT,
            self::STATUS_NOT_ON_SALE,
            self::STATUS_NO_PARTICIPATION,
        ];
    }

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getVisibilityAllowableValues()
    {
        return [
            self::VISIBILITY__PUBLIC,
            self::VISIBILITY__PROTECTED,
            self::VISIBILITY__PRIVATE,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data ?? [], null);
        $this->setIfExists('category_id', $data ?? [], null);
        $this->setIfExists('category', $data ?? [], null);
        $this->setIfExists('status', $data ?? [], null);
        $this->setIfExists('slug', $data ?? [], null);
        $this->setIfExists('name', $data ?? [], null);
        $this->setIfExists('description', $data ?? [], null);
        $this->setIfExists('published', $data ?? [], null);
        $this->setIfExists('visibility', $data ?? [], null);
        $this->setIfExists('authentication_required', $data ?? [], null);
        $this->setIfExists('start', $data ?? [], null);
        $this->setIfExists('end', $data ?? [], null);
        $this->setIfExists('whole_day', $data ?? [], null);
        $this->setIfExists('location', $data ?? [], null);
        $this->setIfExists('num_tickets', $data ?? [], null);
        $this->setIfExists('num_tickets_sold', $data ?? [], null);
        $this->setIfExists('website_url', $data ?? [], null);
        $this->setIfExists('website_subscribe_url', $data ?? [], null);
        $this->setIfExists('comments_open', $data ?? [], null);
        $this->setIfExists('comments', $data ?? [], null);
        $this->setIfExists('media', $data ?? [], null);
        $this->setIfExists('memo', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['category_id'] === null) {
            $invalidProperties[] = "'category_id' can't be null";
        }
        $allowedValues = $this->getStatusAllowableValues();
        if (!is_null($this->container['status']) && !in_array($this->container['status'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'status', must be one of '%s'",
                $this->container['status'],
                implode("', '", $allowedValues)
            );
        }

        if (!is_null($this->container['slug']) && (mb_strlen($this->container['slug']) > 255)) {
            $invalidProperties[] = "invalid value for 'slug', the character length must be smaller than or equal to 255.";
        }

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ((mb_strlen($this->container['name']) > 255)) {
            $invalidProperties[] = "invalid value for 'name', the character length must be smaller than or equal to 255.";
        }

        $allowedValues = $this->getVisibilityAllowableValues();
        if (!is_null($this->container['visibility']) && !in_array($this->container['visibility'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'visibility', must be one of '%s'",
                $this->container['visibility'],
                implode("', '", $allowedValues)
            );
        }

        if (!is_null($this->container['location']) && (mb_strlen($this->container['location']) > 255)) {
            $invalidProperties[] = "invalid value for 'location', the character length must be smaller than or equal to 255.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return self
     */
    public function setId($id)
    {
        if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets category_id
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->container['category_id'];
    }

    /**
     * Sets category_id
     *
     * @param int $category_id category_id
     *
     * @return self
     */
    public function setCategoryId($category_id)
    {
        if (is_null($category_id)) {
            throw new \InvalidArgumentException('non-nullable category_id cannot be null');
        }
        $this->container['category_id'] = $category_id;

        return $this;
    }

    /**
     * Gets category
     *
     * @return \Compucie\Congressus\Model\EventCategoryBase|null
     */
    public function getCategory()
    {
        return $this->container['category'];
    }

    /**
     * Sets category
     *
     * @param \Compucie\Congressus\Model\EventCategoryBase|null $category category
     *
     * @return self
     */
    public function setCategory($category)
    {
        if (is_null($category)) {
            throw new \InvalidArgumentException('non-nullable category cannot be null');
        }
        $this->container['category'] = $category;

        return $this;
    }

    /**
     * Gets status
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string|null $status Status for participating at this event
     *
     * @return self
     */
    public function setStatus($status)
    {
        if (is_null($status)) {
            throw new \InvalidArgumentException('non-nullable status cannot be null');
        }
        $allowedValues = $this->getStatusAllowableValues();
        if (!in_array($status, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'status', must be one of '%s'",
                    $status,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets slug
     *
     * @return string|null
     */
    public function getSlug()
    {
        return $this->container['slug'];
    }

    /**
     * Sets slug
     *
     * @param string|null $slug slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        if (is_null($slug)) {
            array_push($this->openAPINullablesSetToNull, 'slug');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('slug', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        if (!is_null($slug) && (mb_strlen($slug) > 255)) {
            throw new \InvalidArgumentException('invalid length for $slug when calling BaseEvent., must be smaller than or equal to 255.');
        }

        $this->container['slug'] = $slug;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return self
     */
    public function setName($name)
    {
        if (is_null($name)) {
            throw new \InvalidArgumentException('non-nullable name cannot be null');
        }
        if ((mb_strlen($name) > 255)) {
            throw new \InvalidArgumentException('invalid length for $name when calling BaseEvent., must be smaller than or equal to 255.');
        }

        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     *
     * @param string|null $description description
     *
     * @return self
     */
    public function setDescription($description)
    {
        if (is_null($description)) {
            array_push($this->openAPINullablesSetToNull, 'description');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('description', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets published
     *
     * @return bool|null
     */
    public function getPublished()
    {
        return $this->container['published'];
    }

    /**
     * Sets published
     *
     * @param bool|null $published True when this event is published on the website
     *
     * @return self
     */
    public function setPublished($published)
    {
        if (is_null($published)) {
            throw new \InvalidArgumentException('non-nullable published cannot be null');
        }
        $this->container['published'] = $published;

        return $this;
    }

    /**
     * Gets visibility
     *
     * @return string|null
     */
    public function getVisibility()
    {
        return $this->container['visibility'];
    }

    /**
     * Sets visibility
     *
     * @param string|null $visibility Visibility level set for this event
     *
     * @return self
     */
    public function setVisibility($visibility)
    {
        if (is_null($visibility)) {
            throw new \InvalidArgumentException('non-nullable visibility cannot be null');
        }
        $allowedValues = $this->getVisibilityAllowableValues();
        if (!in_array($visibility, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'visibility', must be one of '%s'",
                    $visibility,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['visibility'] = $visibility;

        return $this;
    }

    /**
     * Gets authentication_required
     *
     * @return bool|null
     */
    public function getAuthenticationRequired()
    {
        return $this->container['authentication_required'];
    }

    /**
     * Sets authentication_required
     *
     * @param bool|null $authentication_required True when only authenticated users are allowed to view this event
     *
     * @return self
     */
    public function setAuthenticationRequired($authentication_required)
    {
        if (is_null($authentication_required)) {
            throw new \InvalidArgumentException('non-nullable authentication_required cannot be null');
        }
        $this->container['authentication_required'] = $authentication_required;

        return $this;
    }

    /**
     * Gets start
     *
     * @return \DateTime|null
     */
    public function getStart()
    {
        return $this->container['start'];
    }

    /**
     * Sets start
     *
     * @param \DateTime|null $start start
     *
     * @return self
     */
    public function setStart($start)
    {
        if (is_null($start)) {
            throw new \InvalidArgumentException('non-nullable start cannot be null');
        }
        $this->container['start'] = $start;

        return $this;
    }

    /**
     * Gets end
     *
     * @return \DateTime|null
     */
    public function getEnd()
    {
        return $this->container['end'];
    }

    /**
     * Sets end
     *
     * @param \DateTime|null $end end
     *
     * @return self
     */
    public function setEnd($end)
    {
        if (is_null($end)) {
            throw new \InvalidArgumentException('non-nullable end cannot be null');
        }
        $this->container['end'] = $end;

        return $this;
    }

    /**
     * Gets whole_day
     *
     * @return bool|null
     */
    public function getWholeDay()
    {
        return $this->container['whole_day'];
    }

    /**
     * Sets whole_day
     *
     * @param bool|null $whole_day whole_day
     *
     * @return self
     */
    public function setWholeDay($whole_day)
    {
        if (is_null($whole_day)) {
            array_push($this->openAPINullablesSetToNull, 'whole_day');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('whole_day', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['whole_day'] = $whole_day;

        return $this;
    }

    /**
     * Gets location
     *
     * @return string|null
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     *
     * @param string|null $location location
     *
     * @return self
     */
    public function setLocation($location)
    {
        if (is_null($location)) {
            array_push($this->openAPINullablesSetToNull, 'location');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('location', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        if (!is_null($location) && (mb_strlen($location) > 255)) {
            throw new \InvalidArgumentException('invalid length for $location when calling BaseEvent., must be smaller than or equal to 255.');
        }

        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets num_tickets
     *
     * @return int|null
     */
    public function getNumTickets()
    {
        return $this->container['num_tickets'];
    }

    /**
     * Sets num_tickets
     *
     * @param int|null $num_tickets Capacity for this event. Null means no capacity limit.
     *
     * @return self
     */
    public function setNumTickets($num_tickets)
    {
        if (is_null($num_tickets)) {
            array_push($this->openAPINullablesSetToNull, 'num_tickets');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('num_tickets', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['num_tickets'] = $num_tickets;

        return $this;
    }

    /**
     * Gets num_tickets_sold
     *
     * @return int|null
     */
    public function getNumTicketsSold()
    {
        return $this->container['num_tickets_sold'];
    }

    /**
     * Sets num_tickets_sold
     *
     * @param int|null $num_tickets_sold Number of tickets that are sold for this event
     *
     * @return self
     */
    public function setNumTicketsSold($num_tickets_sold)
    {
        if (is_null($num_tickets_sold)) {
            array_push($this->openAPINullablesSetToNull, 'num_tickets_sold');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('num_tickets_sold', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['num_tickets_sold'] = $num_tickets_sold;

        return $this;
    }

    /**
     * Gets website_url
     *
     * @return string|null
     */
    public function getWebsiteUrl()
    {
        return $this->container['website_url'];
    }

    /**
     * Sets website_url
     *
     * @param string|null $website_url URL for this event on the website. If the association has multiple websites, the first available website on which this event is published, is used.
     *
     * @return self
     */
    public function setWebsiteUrl($website_url)
    {
        if (is_null($website_url)) {
            throw new \InvalidArgumentException('non-nullable website_url cannot be null');
        }
        $this->container['website_url'] = $website_url;

        return $this;
    }

    /**
     * Gets website_subscribe_url
     *
     * @return string|null
     */
    public function getWebsiteSubscribeUrl()
    {
        return $this->container['website_subscribe_url'];
    }

    /**
     * Sets website_subscribe_url
     *
     * @param string|null $website_subscribe_url URL on the website to subscribe for this event. If the association has multiple websites, the first available website on which this event is published, is used.
     *
     * @return self
     */
    public function setWebsiteSubscribeUrl($website_subscribe_url)
    {
        if (is_null($website_subscribe_url)) {
            throw new \InvalidArgumentException('non-nullable website_subscribe_url cannot be null');
        }
        $this->container['website_subscribe_url'] = $website_subscribe_url;

        return $this;
    }

    /**
     * Gets comments_open
     *
     * @return bool|null
     */
    public function getCommentsOpen()
    {
        return $this->container['comments_open'];
    }

    /**
     * Sets comments_open
     *
     * @param bool|null $comments_open comments_open
     *
     * @return self
     */
    public function setCommentsOpen($comments_open)
    {
        if (is_null($comments_open)) {
            array_push($this->openAPINullablesSetToNull, 'comments_open');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('comments_open', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['comments_open'] = $comments_open;

        return $this;
    }

    /**
     * Gets comments
     *
     * @return \Compucie\Congressus\Model\EventComment[]|null
     */
    public function getComments()
    {
        return $this->container['comments'];
    }

    /**
     * Sets comments
     *
     * @param \Compucie\Congressus\Model\EventComment[]|null $comments comments
     *
     * @return self
     */
    public function setComments($comments)
    {
        if (is_null($comments)) {
            array_push($this->openAPINullablesSetToNull, 'comments');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('comments', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['comments'] = $comments;

        return $this;
    }

    /**
     * Gets media
     *
     * @return \Compucie\Congressus\Model\StorageObject[]|null
     */
    public function getMedia()
    {
        return $this->container['media'];
    }

    /**
     * Sets media
     *
     * @param \Compucie\Congressus\Model\StorageObject[]|null $media media
     *
     * @return self
     */
    public function setMedia($media)
    {
        if (is_null($media)) {
            array_push($this->openAPINullablesSetToNull, 'media');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('media', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }
        $this->container['media'] = $media;

        return $this;
    }

    /**
     * Gets memo
     *
     * @return string|null
     */
    public function getMemo()
    {
        return $this->container['memo'];
    }

    /**
     * Sets memo
     *
     * @param string|null $memo Internal notes for this event
     *
     * @return self
     */
    public function setMemo($memo)
    {
        if (is_null($memo)) {
            throw new \InvalidArgumentException('non-nullable memo cannot be null');
        }
        $this->container['memo'] = $memo;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


